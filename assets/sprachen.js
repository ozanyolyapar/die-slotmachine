/**
 * Created by Ozan on 14.06.2015.
 */

var langs = {
    de: {
        spiel: 'Spiel <span class="caret"></span>',
        restart: 'Spiel neustarten',
        impressum: 'Impressum',
        howto: 'Anleitung',
        werist: 'Wer ist Ozan?',
        hell: 'Hell',
        dunkel: 'Dunkel',
        codebybucket: 'Code auf Bitbucket',
        close: 'Schließen',
        anleitungtext: 'Anfangsbedingungen: 500<i class="ofont-coin-dollar"></i> und 2 Minuten Zeit.' +
        'Das Spiel ist sehr leicht Aufgebaut: <br>' +
            '<ol>' +
                '<li>Starte das Spiel.</li>' +
                '<li>Drücke die <strong>Leertaste</strong> oder den "Los"-Button um zu ziehen.</li>' +
                '<li>Warte bis die Drehmechanismen stoppen.</li>' +
                '<li>Du bekommst deine Coins.</li>' +
                '<li>Widerhole Schritt 2</li>' +
            '</ol>' +
            'Bei jedem Zug ohne Treffer werden dir <strong>50 coins</strong> abgezogen. <br>Du bekommst:' +
            '<ul>' +
                '<li>für <strong>2</strong> Treffer +50coins</li>' +
                '<li>für <strong>3</strong> Treffer +150coins</li>' +
            '</ul>' +
            'In der unteren Leiste sind Informationen, wie Anzahl der Drehungen (<i class="ofont-spinner11"></i>), ' +
            'derzeitiger Creditstand/letzte Kontoänderung(<i class="ofont-coin-dollar"></i>) und die ' +
            'verbleibende Zeit(<i class="ofont-stopwatch"></i>), vorhanden.',
        spielzuende: 'Winner winner, chicken din.. Oh, Spiel zu Ende!',
        zeitzuende: 'Deine Zeit ist abgelaufen, Kumpel! Du kannst es gerne erneut versuchen.',
        coinszuende: 'Du hast keine Credits mehr! Schade.. Versuch\'s erneut!',
        starttext: 'Klicke auf <i class="ofont-play3"></i> und das Spiel startet.<br>Du hast 2 Minuten und 500 Coins.',
        los: 'Los!' // go
    },
    en: {
        spiel: 'Game <span class="caret"></span>',
        restart: 'Restart game',
        impressum: 'Impress',
        howto: 'How To',
        werist: 'Who is Ozan?',
        hell: 'Light',
        dunkel: 'Dark',
        codebybucket: 'Code at Bitbucket',
        close: 'Close',
        anleitungtext: 'You get: 500<i class="ofont-coin-dollar"></i> and 2 minutes.' +
                        'Step-by-step guide: <br>' +
                        '<ol>' +
                            '<li>Start the game.</li>' +
                            '<li>Press <strong>space bar</strong> or "Go" button to spin.</li>' +
                            '<li>Wait until the animation ends.</li>' +
                            '<li>You get coins.</li>' +
                            '<li>Repeat Step 2</li>' +
                        '</ol>' +
                        'You pay <strong>50 coins</strong> for every turn. <br>You get:' +
                        '<ul>' +
                            '<li> +50coins for <strong>2</strong> matches</li>' +
                            '<li> +150coins for <strong>3</strong> matches</li>' +
                        '</ul>' +
                        'The bottom bar is full-filled with informations such as the number of turns (<i class="ofont-spinner11"></i>), ' +
                        'current credit status/last change(<i class="ofont-coin-dollar"></i>) and ' +
                        'remaining time(<i class="ofont-stopwatch"></i>).',
        spielzuende: 'End of the line!',
        zeitzuende: 'Your time is up, buddy! Try again.',
        coinszuende: 'Winner winner, chicken din.. Oh, watch up your credits!',
        starttext: 'Tap to <i class="ofont-play3"></i> to start the game.<br>You get 2 minutes and 500 coins.',
        los: 'Go!' // go
    }
}
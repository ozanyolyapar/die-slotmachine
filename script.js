var elements = {
    overlay: '.overlay',
    coins: '.coins',
    time: '.time',
    repeats: '.repeats',
    spin1: '.spin1',
    spin2: '.spin2',
    spin3: '.spin3',
    spin1Choose: '.spin1-choose',
    spin2Choose: '.spin2-choose',
    spin3Choose: '.spin3-choose',
    plusminus: '.derzeitigepointanzeige'
};
var symbols = [
    'spades',
    'clubs',
    'diamonds',
    'pacman',
    'bug',
    'fire'
];

var current = [
    -1,0,0,0
];

var currentLang = "de";
var endofline = true;
var reset = false;
var animationLaeuft = false;

function _get(element){
    return $(elements[element]);
}

var hideElements = {
    current: '',
    time: 300
};
function hide(obj){
    hideElements.current = obj;
    hideElements.current.addClass('hide-animate');
    setTimeout(function(){
        hideElements.current.addClass('hide');
    },hideElements.time);
}

function show(obj){
    hideElements.current = obj;
    hideElements.current.removeClass('hide');
    setTimeout(function(){
        hideElements.current.removeClass('hide-animate');
    },hideElements.time);
}

function animatePlusMinus(value){
    var obj = _get('plusminus');
    obj.removeClass('show');
    obj.attr('data-tooltip',value);
    obj.addClass('show');
    setTimeout(function () {
        _get('plusminus').removeClass('show');
    }, 2000);
}

function onresizeEventCatcher(){
    //alert('sort begin')
    var offset = $('.spin.first i:nth-child(2)').offset();
    $('.derzeitigepointanzeige').css({
        left: offset.left-125+'px'
    })
}

function legeZeichenFest(){
    _get('spin1Choose').removeClass('ofont-'+current[1]);
    _get('spin2Choose').removeClass('ofont-'+current[2]);
    _get('spin3Choose').removeClass('ofont-'+current[3]);
    current[1]=symbols[Math.floor((Math.random() * symbols.length))];
    current[2]=symbols[Math.floor((Math.random() * symbols.length))];
    current[3]=symbols[Math.floor((Math.random() * symbols.length))];
    _get('spin1Choose').addClass('ofont-'+current[1]);
    _get('spin2Choose').addClass('ofont-'+current[2]);
    _get('spin3Choose').addClass('ofont-'+current[3]);
}

var timeOd;
var timeCount = 120;
var versuche = {
    odo: 0,
    count: 0
};
var coins = {
    odo: 0,
    count: 500
};
var last = {
    odo: 0,
    count: 0
};

var userLang;

$(window).on('resize', onresizeEventCatcher);

function loadLanguage(langcode){
    currentLang=langcode;
    var $langdata = langs[langcode];

    $('.hebel-btn').html($langdata.los);

    $('.nav-game').html($langdata.spiel);
    $('.nav-restart').html($langdata.restart);
    $('.nav-impress').html($langdata.impressum);
    $('.nav-howto').html($langdata.howto);
    $('.nav-whois').html($langdata.werist);

    $('.nav-hell').html($langdata.hell);
    $('.nav-dunkel').html($langdata.dunkel);

    $('.nav-codeat').html($langdata.codebybucket);

    $('#modalHowto .modal-body').html($langdata.anleitungtext);
    $('#modalHowtoLabel').html($langdata.howto);

    $('#modalEndOfTheGameLabel').html($langdata.spielzuende);
    $('.zeitaus').html($langdata.zeitzuende);
    $('.coinsaus').html($langdata.coinszuende);

    $('.modal-footer button.btn-schliessen[data-dismiss="modal"]').html($langdata.close);

    $('.erklaerung').html(
        $langdata.starttext+
            '<br/><span class="fsxl"><a href="#" data-toggle="modal" data-target="#modalHowto">'+$langdata.howto+'</a></span>'
    );
}

$(document).ready(function() {
    onresizeEventCatcher();
    window.odometerOptions = {
        auto: false,
        duration: 10
    };

    userLang = navigator.language || navigator.userLanguage;
    loadLanguage('de');

    $('body').addClass('ozan-did-it').addClass($.browser.name);

    if($.browser.firefox) $('body').addClass('firefox')

    timeOd = new Odometer({
        el: $('.time-class')[0],
        value: timeCount,
        duration: 10,
        format: '',
        theme: 'default'
    });

    versuche.odo = new Odometer({
        el: $('.repeats')[0],
        value: versuche.count,
        duration: 10,
        format: '',
        theme: 'default'
    });

    coins.odo = new Odometer({
        el: $('.coins')[0],
        value: coins.count,
        duration: 3000,
        format: '',
        theme: 'default'
    });

    last.odo = new Odometer({
        el: $('.last')[0],
        value: last.count,
        duration: 3000,
        format: '',
        theme: 'default'
    });

    $(".spin1 .spin-inner").bind('oanimationend animationend webkitAnimationEnd', function() {
        if(!window.reset){
            $(".spin1 .spin-inner").addClass('end');
            $('.spin2 .spin-inner').addClass('dreh');
        }
    });
    $(".spin2 .spin-inner").bind('oanimationend animationend webkitAnimationEnd', function() {
        if(!window.reset) {
            $(".spin2 .spin-inner").addClass('end');
            $('.spin3 .spin-inner').addClass('dreh');
        }
    });
    $(".spin3 .spin-inner").bind('oanimationend animationend webkitAnimationEnd', function() {
        if(!window.reset) {
            $(".spin3 .spin-inner").addClass('end');
            bewerte();
        }
    });
    $(window).keypress(function(e) {
        if ((e.keyCode == 0 || e.keyCode == 32) && !$('.hebel-btn').is(':focus')) {
            zieheHebel();
        }
    });

    $('.btn-toggle').click(function() {
        $(this).find('.btn').toggleClass('active');

        if ($(this).find('.btn-primary').size()>0) {
            $(this).find('.btn').toggleClass('btn-primary');
        }
        if ($(this).find('.btn-danger').size()>0) {
            $(this).find('.btn').toggleClass('btn-danger');
        }
        if ($(this).find('.btn-success').size()>0) {
            $(this).find('.btn').toggleClass('btn-success');
        }
        if ($(this).find('.btn-info').size()>0) {
            $(this).find('.btn').toggleClass('btn-info');
        }
        if ($(this).find('.toggle-theme').size()>0) {
            //$(this).find('.btn').toggleClass('btn-info');
            toggleTheme();
        }
        if ($(this).find('.toggle-lang').size()>0) {
            //$(this).find('.btn').toggleClass('btn-info');

        }

        $(this).find('.btn').toggleClass('btn-default');
    });
    $('.btn-toggle.toggle-theme').click(function() {
        toggleTheme();
    });
    $('.btn-toggle.toggle-lang').click(function() {
        if(currentLang=="de"){
            loadLanguage("en")
        }else if(currentLang=="en"){
            loadLanguage("de")
        }
    });
});
var theme = "hell"
function toggleTheme(){
    $('body').toggleClass('dunkel');

    //if(theme=="hell"){
    //    $('body').
    //    theme="dunkel";
    //}else if(theme=="dunkel"){
    //
    //
    //    theme="hell";
    //}
}

function zieheHebel(){
    if(!endofline && !animationLaeuft){
        if(coins.count>0){
            resetAnimation(function(){
                legeZeichenFest();
                versuche.count++;
                versuche.odo.update(versuche.count);
                $('.spin1 .spin-inner').addClass('dreh');
                animationLaeuft=true;
            });
        }else{
            endeAlles(1);
        }
    }
}

function endeAlles(variante){
    if(variante==1){
        $('#modalEndOfTheGame .zeitaus').addClass('hide');
        $('#modalEndOfTheGame .coinsaus').removeClass('hide');
        $('#modalEndOfTheGame').modal('toggle');
    }else if(variante==2){
        $('#modalEndOfTheGame .zeitaus').removeClass('hide');
        $('#modalEndOfTheGame .coinsaus').addClass('hide');
        $('#modalEndOfTheGame').modal('toggle');
    }
    $('.endeversuche').text(versuche.count);
    $('.endecredits').text(coins.count);
    endofline = true;
    clearInterval(timeout1);
}

function resetAnimation(callback){
    window.reset=true;

    //$('.spin1 .spin-inner').addClass('reset');
    //$('.spin2 .spin-inner').addClass('reset');
    //$('.spin3 .spin-inner').addClass('reset');

    setTimeout(function(){
        $('.spin1 .spin-inner').removeClass('dreh');
        $('.spin2 .spin-inner').removeClass('dreh');
        $('.spin3 .spin-inner').removeClass('dreh');
        $('.spin1 .spin-inner').removeClass('end');
        $('.spin2 .spin-inner').removeClass('end');
        $('.spin3 .spin-inner').removeClass('end');
        checker([-1]);
    }, 200);

    setTimeout(callback, 300);
    window.reset=false;
}

function checker(array){
    if(array[0]==-1){
        _get('spin1Choose').removeClass('checked');
        _get('spin2Choose').removeClass('checked');
        _get('spin3Choose').removeClass('checked');
    }else{
        for(val in array){
            _get('spin'+array[val]+'Choose').addClass('checked');
        }
    }
}

function bewerte(){
    var one = current[1];
    var two = current[2];
    var three = current[3];
    animationLaeuft=false;

    if(one==two && two==three){
        addPoints(150);
        checker([1,2,3]);
    }else if((one==two) || (one==three) || (two==three)){ // zwei richtige
        addPoints(50);
        // färben
        if(one==two){
            checker([1,2]);
        }else if(one==three){
            checker([1,3]);
        }else if(two==three){
            checker([2,3]);
        }
        // färben ende
    }else{
        addPoints(-50);
    }
}

function addPoints(points){
    last.odo.update(points);
    var pref="";
    if(points>0) pref="+"; else pref="";
    animatePlusMinus(pref+points);
    coins.count+=points;
    coins.odo.update(coins.count);
}

function startTheFFFGame(){
    endofline=false;
    hide(_get('overlay'));
    timeOd;
    timeCount = 120;
    versuche = {
        odo: 0,
        count: 0
    }
    coins = {
        odo: 0,
        count: 500
    }
    last = {
        odo: 0,
        count: 0
    }

    window.odometerOptions = {
        auto: false,
        duration: 10
    };

    timeOd = new Odometer({
        el: $('.time-class')[0],
        value: timeCount,
        duration: 10,
        format: '',
        theme: 'default'
    });

    versuche.odo = new Odometer({
        el: $('.repeats')[0],
        value: versuche.count,
        duration: 10,
        format: '',
        theme: 'default'
    });

    coins.odo = new Odometer({
        el: $('.coins')[0],
        value: coins.count,
        duration: 3000,
        format: '',
        theme: 'default'
    });

    last.odo = new Odometer({
        el: $('.last')[0],
        value: last.count,
        duration: 3000,
        format: '',
        theme: 'default'
    });
    clearInterval(timeout1);

    setTimeout(function(){
        zieheHebel();
    }, 500);
    updateTime();
}

var timeout1;
function updateTime(){
    timeout1 = setInterval(function() {
        if(timeCount > 0){
            timeCount--;
            timeOd.update(timeCount);
        }else{
            endeAlles(2);
        }
    }, 1000);
}